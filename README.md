#Recipie App API PROXY

NGINX proxy app for our recipe app API

##Usage

##Environment variables

* `LISTEN_PORT` -port to listen on (default `8000`)
* `APP_HOST` - Hostname of the app to forward requests to default: `app`)
* `APP_PORT` - PORT OF THE APP TO listen on (default `8000`)
